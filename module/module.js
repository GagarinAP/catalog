var fs = require('fs');
var _ = require('lodash');

module.exports = (function () {

    var dbFilePath = './data/objects.json';

    var getObjectsFromFile = function (path) {
      try {
        var result = fs.readFileSync(path, 'utf8');
        return JSON.parse(result);
      } catch(e) {           
        return [];
      }
    };
    
    var data = getObjectsFromFile(dbFilePath);
    
    var DisplayAll = function() {
      var result = [];      
      for (var i = 0; i < data.length; ++i) {
        result.push(data[i]);
      }        
      return result;      
    };  

    var DisplayId = function(id) {
      var result = [];     
      for (var i = 0; i < data.length; ++i) {
        if(data[i].id == parseInt(id)){
          result.push(data[i]);
        }
      }        
      return result;      
    }; 

    var SearchByCity = function(city){
      var result = [];
      for(var i = 0; i < data.length; ++i) {
        if(data[i].city == city) {
            result.push(data[i]);
        }
      }      
      return result;      
    };
    
    return {
        DisplayAll: DisplayAll,
        DisplayId: DisplayId,
        SearchByCity: SearchByCity        
    };

})();