var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var module = require('./module/module.js');//подключаем логику
var _ = require('lodash');
var app = express();

app.set('port', (process.env.PORT || 3000));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
//Роутеры рендер страниц
app.get('/', function(req, res) {
  res.render('index',{title: 'Main Page'});  
});

app.get('/catalog', function(req, res) {
  res.render('catalog',{title: 'Catalog Page'});  
});

app.get('/search', function(req, res) {
  res.render('search',{title: 'Search Page'});  
});

//Роутеры для аякс -> бекенд
app.get('/all', function(req, res) {
  res.send(module.DisplayAll());  
});

app.get('/catalog/:id', function(req, res) {
  res.send(module.DisplayId(req.params.id));  
});

app.get('/search/:city', function (req, res) {
  res.send(module.SearchByCity(req.params.city));
});

app.listen(app.get('port'), function() {
  
});
