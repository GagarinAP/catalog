$(function () {
    window.module = (function () {
        var init = function(event) {
            var id = location.search.split('id=')[1];
            var name = location.search.split('city=')[1];        
            $.ajax('/all').done(DisplayAll);                   
            $.ajax('/catalog/'+id).done(DisplayId);
            $.ajax('/search/'+name).done(SearchOutput);           
        };
        var DisplayAll = function(data) {            
            $.each(data, function (i, value) {
                if(value.logo === null){
                    value.logo = '/img/notfound.png';
                }
                var list = '<li class="hidden list-group-item"><div class="row"><div class="col-lg-1 col-md-1 col-sm-2 col-xs-2"><img width="64" height="64" class="img-responsive" src="'+value.logo+'"></div><div class="col-lg-10 col-md-10 col-sm-8 col-xs-8"><h4 class="list-group-item-heading">'+value.name+'</h4><p class="list-group-item-text">Rating: '+value.rating+'</p></div><div class="col-lg-1 col-md-1 col-sm-2 col-xs-2"><a class="btn btn-info" role="button" href="/catalog?id='+value.id+'"><span class="glyphicon glyphicon-triangle-right"></span></a></li></div></div>';
                $('#DisplayAll').append(list);
            });
                
            function loadMore(){
                $("#DisplayAll .hidden").slice(0,10).removeClass("hidden");
            }                
            loadMore();                
            $("#btnLoadMore").on("click",loadMore);
        };

        var DisplayId = function(data) {
            var result = '';
            if(data[0].logo === null){
                data[0].logo = '/img/notfound.png';
            }            
            for (var i = 0; i < data.length; ++i) { 
                result += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><div class="thumbnail"><img class="img-responsive" src="'+data[0].logo+'">';                               
                result += '<div class="caption"><h2>'+data[0].name+'</h2>';                
                result += '<p>Rating: '+data[0].rating+'</p>';
                result += '<p>Country Code: '+data[0].countryCode+'</p>';
                result += '<p>Address: '+data[0].address+'</p>';
                result += '<p>Postal Code: '+data[0].postalCode+'</p>';
                result += '<p>City: '+data[0].city+'</p>';
                result += '<p>Region Code: '+data[0].regionCode+'</p>';                                
                result += '<a class="btn btn-info" role="button" href="/">Back</a></div></div>';                                   
            }
            $('#DisplayId').html(result);
        };

        var SearchOutput = function (data) {
            var city = location.search.split('city=')[1];
            if (!data || data.length === 0) {
                $('#SearchOutput').html('<h3><strong>Nothing to found for city: ' + city + '</strong></h3>');
                return;
            }
            var result = '<h3>Search for city ' + city + ' found: </h3>';
            data.sort(function (a, b) {
              if (a.name > b.name) { return 1; }
              if (a.name < b.name) { return -1; }
              return 0;
            });
            for (var i = 0; i < data.length; ++i) {
                result += '<a href="/catalog?id=' + data[i].id + '"><h4>' + data[i].name + '</h4></a>';
            }           
            $('#SearchOutput').html(result);
        };        

        init();              
    })();   
});